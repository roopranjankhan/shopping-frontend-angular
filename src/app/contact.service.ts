import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Contact} from './contact';


@Injectable({
  providedIn: 'root'
})

@Injectable()
export class ContactService {

  private postContactUrl: string;

  constructor(private http: HttpClient) {
    this.postContactUrl = 'http://localhost:8080/feedback';
  }

  public save(contact: Contact) {
    return this.http.post<Contact>(this.postContactUrl, contact);
  }
}
