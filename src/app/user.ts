export class User {
  id: string;
  name: string;
  username: string;
  password: string;
  emailAddress: string;
  age: number;
  phoneNumber: string;
}
