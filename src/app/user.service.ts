import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from './user';

@Injectable({
  providedIn: 'root'
})

@Injectable()
export class UserService {

  private postUsersUrl: string;

  constructor(private http: HttpClient) {
    this.postUsersUrl = 'http://localhost:8080/login';
  }

  public save(user: User) {
    return this.http.post<User>(this.postUsersUrl, user);
  }

}
