export class Records {
  id: number;
  name: string;
  price: number;
  imageUrl: string;

  constructor(id, name, price = 0, imageUrl = '') {
    this.id = id;
    this.name = name;
    this.price = price;
    this.imageUrl = imageUrl;
  }
}
