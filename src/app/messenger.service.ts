import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

@Injectable()
export class MessengerService {

  subject = new Subject();

  constructor() {
  }

  sendMsg(record) {
    this.subject.next(record);
  }

  getMsg() {
    return this.subject.asObservable();
  }

}
