import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ContactService} from '../contact.service';
import {Contact} from '../contact';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contact: Contact;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private contactService: ContactService) {

    this.contact = new Contact();
  }

  onSubmit() {
    this.contactService.save(this.contact).subscribe(result => this.goToContactList());
    alert('Message submitted');
  }

  goToContactList() {
    this.router.navigate(['/contact']);
  }

  ngOnInit(): void {
  }

}
