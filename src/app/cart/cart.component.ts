import {Component, Input, OnInit} from '@angular/core';
import {MessengerService} from '../messenger.service';
import {Records} from '../model/records';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cartItems = [];

  cartTotal = 0;

  constructor(private msg: MessengerService) {
  }

  ngOnInit() {
    this.msg.getMsg().subscribe((record: Records) => {
      this.addProductToCart(record);
    });
  }

  addProductToCart(record: Records) {

    let recordExists = false;

    for (const i in this.cartItems) {
      if (this.cartItems[i].recordId === record.id) {
        this.cartItems[i].qty++;
        recordExists = true;
        break;
      }
    }

    if (!recordExists) {
      this.cartItems.push({
        recordId: record.id,
        recordName: record.name,
        qty: 1,
        price: record.price,
        imageUrl: record.imageUrl
      });
    }


    this.cartTotal = 0;
    this.cartItems.forEach(item => {
      this.cartTotal += (item.qty * item.price);
    });
  }

}
