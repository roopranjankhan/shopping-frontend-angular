export class Icon {
  iconUrl: string;
  link: string;

  constructor(iconUrl = '', link) {
    this.iconUrl = iconUrl;
    this.link = link;
  }
}
