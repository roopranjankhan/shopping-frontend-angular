import {Injectable} from '@angular/core';
import {Records} from './model/records';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

@Injectable()
export class RecordsService {

  private getRecordsUrl: string;

  constructor(private http: HttpClient) {
    this.getRecordsUrl = 'http://localhost:8080/product/all';
  }

  public findAll(): Observable<Records[]> {
    return this.http.get<Records[]>(this.getRecordsUrl);
  }

}
