import {Component, Input, OnInit} from '@angular/core';
import {Records} from '../model/records';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.css']
})
export class CartItemComponent implements OnInit {

  constructor() {
  }

  @Input() cartItem: any;

  cartTotal = 0;

  qty = 0;

  ngOnInit() {
  }


  addProductToCart(record: Records) {
    if (this.cartItem.push(record)) {
      this.cartTotal += record.price;
      console.log(this.cartTotal);
      console.log(this.qty++);
    }
  }
}
