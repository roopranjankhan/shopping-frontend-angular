import {Injectable} from '@angular/core';
import {Icon} from './icon';

@Injectable({
  providedIn: 'root'
})

@Injectable()
export class IconService {

  constructor() {
  }

  icons: Icon[] = [
    new Icon('https://cdn2.iconfinder.com/data/icons/website-icons/512/User_Avatar-512.png', 'home'),
    new Icon('https://clipartart.com/images/clipart-contact-2.jpg', 'contact'),
    new Icon('https://cdn2.iconfinder.com/data/icons/e-commerce-line-4-1/1024/open_box4-512.png', 'product-list'),
    new Icon('https://cdn1.vectorstock.com/i/1000x1000/25/25/shopping-cart-icon-vector-12712525.jpg', 'cart'),
  ];

  getIcons(): Icon[] {
    return this.icons;
  }
}
