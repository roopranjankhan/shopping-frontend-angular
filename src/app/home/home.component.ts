import {Component, OnInit} from '@angular/core';
import {User} from '../user';
import {UserService} from '../user.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  user: User;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private userServiceService: UserService) {

    this.user = new User();
  }


  onSubmit() {
    this.userServiceService.save(this.user).subscribe(result =>
      this.goToUsersList()
    );
    alert('Successfully registered');
  }


  goToUsersList() {
    this.router.navigate(['/sign']);
  }

  ngOnInit(): void {
  }

}
