import {Component, Input, OnInit} from '@angular/core';
import {Icon} from '../icon';
import {IconService} from '../icon.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  iconList: Icon[] = [];

  links = ['home', 'contact', 'products'];

  selectedPage = 'home';


  changeSelectedPageMenuColour(selectedMenu) {
    this.selectedPage = selectedMenu;
  }

  constructor(private iconService: IconService) {
  }

  ngOnInit() {
    this.iconList = this.iconService.getIcons();
  }

}
