import {Component, Input, OnInit} from '@angular/core';
import {Records} from '../model/records';
import {RecordsService} from '../records.service';
import {MessengerService} from '../messenger.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {


  recording: Records[] = [];

  searchText = '';

  cartTotal = 0;

  qty = 0;


  constructor(private record: RecordsService) {
  }

  ngOnInit() {
    this.record.findAll().subscribe(data => {
      this.recording = data;
    });
  }


  addProductToCart(record: Records) {


    console.log(this.cartTotal += record.price * this.qty++);

  }
}
