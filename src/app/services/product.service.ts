import {Injectable} from '@angular/core';
import {Product} from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

    products: Product[] = [
      new Product(1, 'Denim', 25, 'C:\\Users\\37254\\Pictures\\denim.png'),
      new Product(2, 'Shirt', 30.50, 'C:\\Users\\37254\\Pictures\\white.png'),
    ];

    getProducts(): Product[] {
      return this.products;
    }

}
